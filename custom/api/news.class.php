<?php
import("Custom.Api.apiBase");
import("Custom.Data.userMode");
import("custom.data.newsMode");
class news extends apiBase {
    /** @var  userMode */
    protected $user;
    /** @var  newsMode */
    protected $news;

    protected function __construct() {
        parent::__construct();
        $this->user=userMode::init();
        $this->news=newsMode::init();
    }

    protected function listTask(){
        $rows=isset($_GET['rows'])&&!empty($_GET['rows'])?$_GET['rows']:20;
        $offset=isset($_GET['offset'])&&!empty($_GET['offset'])?$_GET['offset']:0;
        $ifArticle=isset($_GET['ifArticle'])&&$_GET['ifArticle']?true:false;
        $ifVoice=isset($_GET['ifVoice'])&&$_GET['ifVoice']?true:false;
        if($ifArticle){
            list($ids,$r['total'])=$this->news->getList($rows,$offset);
            $r['list']=$this->news->content($ids,$ifArticle,$ifVoice);
        }else{
            list($r['list'],$r['total'])=$this->news->simpleList($rows,$offset);
        }
        $r['count']=count($r['list']);

        $result['data']=$r;
        $result['message']="success";
        $result['state']=$r['count']>0?200:204;
        View::displayAsJson($result);
    }
}
