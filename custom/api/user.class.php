<?php
import("Custom.Api.apiBase");
import("Custom.Data.userMode");
class user extends apiBase {
    /** @var  userMode */
    protected $user;

    protected function __construct() {
        parent::__construct();
        $this->user=userMode::init();
    }

    function login($user, $pass){
        if(strlen($pass)!=32){
            $r['message']="密码未经过客户端加密";
            $r['state']=403;
            return $r;
        }
        $r['message']=$this->user->login($user,$pass,userMode::LOGIN_EXPAND_NOT);
        if(strtolower($r['message'])=="success"){
            $r['data']=$this->user->getBaseInfo();
            $r['data']['token']=$this->getToken();
            $r['state']=200;
        }else{
            $r['state']=401;
        }
        return $r;
    }
    function loginTask(){
        if(isset($_GET['user'])&&!empty($_GET['user'])&& isset($_GET['pass'])&&!empty($_GET['pass'])){
            $r=$this->login($_GET['user'],$_GET['pass']);
        }else{
            $r['message']="账号或者用户名为空";
            $r['state']=400;
        }
        View::displayAsJson($r);
    }

    function checkUserTask(){
        if(isset($_GET['user'])&&!empty($_GET['user'])){
            $user=$_GET['user'];
            $r['exist']=$this->user->checkUser($user);
            $r['state']=200;
        }else{
            $r['message']="账号为空";
            $r['state']=400;
        }
        View::displayAsJson($r);
    }

    function judgeTelTask(){
        if(isset($_GET['tel'])&&!empty($_GET['tel'])){
            $tel=$_GET['tel'];
            $r['user']=$this->user->judgeTel($tel);
            $r['exist']=$r['user']?true:false;
            $r['state']=200;
        }else{
            $r['message']="电话为空";
            $r['state']=400;
        }
        View::displayAsJson($r);
    }

    function judgeEmailTask(){
        if(isset($_GET['email'])&&!empty($_GET['email'])){
            $email=$_GET['email'];
            $r['user']=$this->user->judgeEmail($email);
            $r['exist']=$r['user']?true:false;
            $r['state']=200;
        }else{
            $r['message']="邮箱为空";
            $r['state']=400;
        }
        View::displayAsJson($r);
    }

    function register($user, $pass, $tel, $email){
        if($this->user->checkUser($user)){
            $r['message']="用户名已经存在";
            $r['state']=403;
            return $r;
        }
        if(strlen($pass)!=32){
            $r['message']="密码未经过客户端加密";
            $r['state']=403;
            return $r;
        }
        if(!empty($tel)&& $this->user->judgeTel($tel)!==null){
            $r['message']="电话已经存在";
            $r['state']=403;
            return $r;
        }
        if(!empty($email)&& $this->user->judgeEmail($email)!==null){
            $r['message']="邮箱已经存在";
            $r['state']=403;
            return $r;
        }

        if($this->user->register($user, $pass, $tel, $email)){
            $r['message']="success";
            $r['state']=200;
        }else{
            $r['message']="未知错误原因，请与管理员联系";
            $r['state']=500;
        }
        return $r;
    }

    function registerTask(){
        if(!isset($_GET['user'])||!isset($_GET['pass'])){
            $r['message']="账号或者用户名为空";
            $r['state']=400;
        }else{
            $user=$_GET['user'];
            $pass=$_GET['pass'];
            $tel=isset($_GET['tel'])?$_GET['tel']:null;
            $email=isset($_GET['email'])?$_GET['email']:null;
            $r=$this->register($user, $pass, $tel, $email);
        }
        View::displayAsJson($r);
    }
}
