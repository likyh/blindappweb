<?php
class apiBase extends Activity {
    protected function __construct() {
        SimpleSession::init(null,isset($_GET['token'])?$_GET['token']:null);
    }

    protected function getToken(){
        return session_id();
    }
}