<?php
/**
 * Created by PhpStorm.
 * User: linyh
 * Date: 2014/9/22
 * Time: 13:21
 */

class voiceMode extends Data {
    function content($ids){
        if(empty($ids)){
            return null;
        }
        $idCondition=$this->db->getIdCondition($ids);
        $sql="select * from voice where {$idCondition}";
        return $this->db->getAll($sql);
    }

    function modify($id=null, $title, $url){
        $data['title']=$this->db->quote($title);
        $data['url']=$this->db->quote($url);
        if(!empty($id)){
            $re=$this->db->update("voice",$id,$data);
        }else{
            $re=$this->db->insert("voice",$data);
        }
        return $re>0;
    }

    function delete($id){
        $this->db->delete("voice",$id);
    }
} 