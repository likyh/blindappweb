<?php
/**
 * Created by PhpStorm.
 * User: linyh
 * Date: 2014/9/22
 * Time: 13:20
 */

import("custom.data.voiceMode");
class newsMode extends Data {
    function simpleList($rows, $offset=0){
        $rows=(int)$rows;
        $offset=(int)$offset;
        $sql="select * from news where 1 limit $offset,$rows";
        $totalSql="select count(1) from news where 1";
        $data=$this->db->getAll($sql);
        $total=$this->db->getValue($totalSql);
        return array($data,$total);
    }

    function getList($rows, $offset=0,$text=null){
        $rows=(int)$rows;
        $offset=(int)$offset;

        $sql="select id from news where 1 limit $offset,$rows";
        $tempData=$this->db->getAll($sql);
        $data=array();
        foreach($tempData as $v){
            $data[]=$v['id'];
        }

        $totalSql="select count(1) from news where 1";
        $total=$this->db->getValue($totalSql);
        return array($data,$total);
    }

    function content($ids,$ifArticle,$ifVoice){
        if(empty($ids)){
            return null;
        }
        $idCondition=$this->db->getIdCondition($ids);

        if($ifArticle){
            //TODO 这里直接把news与news_article一对多的关系当做一对一来处理了，以后要修改可以考虑在建立一个newsArticleMode
            $sql="select A.id as news_article_id,A.content,A.voice_ids,news.* from news,news_article A
where news.id=A.news_id and news.{$idCondition}";
        }else{
            $sql="select * from news where {$idCondition}";
        }
        $data=$this->db->getAll($sql);
        if($ifArticle&&$ifVoice){
            $voice=voiceMode::init();
            if(!$voice instanceof voiceMode) trigger_error("voiceMode类初始化失败");
            foreach($data as &$v){
                if(!empty($v['voice_ids'])){
                    $v['voice']=$voice->content(explode(",",$v['voice_ids']));
                }else{
                    $v['voice']=array();
                }
            }
        }

        return count($data)>1?$data:$data[0];
    }
}