<?php
/**
 * Created by PhpStorm.
 * User: linyh
 * Date: 2014/9/22
 * Time: 13:15
 */

class userMode extends Data {
    const LOGIN_EXPAND=true;
    const LOGIN_EXPAND_NOT=false;

    protected function __construct() {
        parent::__construct();
        $this->session=SimpleSession::init();
    }

    function checkUser($user){
        $user=$this->db->quote($user);
        $sql="select id from user where `user`=$user";
        return $this->db->getExist($sql);
    }

    function judgeTel($tel){
        $tel=$this->db->quote($tel);
        $sql="select user from user where `tel`=$tel";
        return $this->db->getValue($sql);
    }

    function judgeEmail($email){
        $email=$this->db->quote($email);
        $sql="select user from user where `email`=$email";
        return $this->db->getValue($sql);
    }

    /**
     * 密码分为客户端和服务器端两次加密，这个是客户端加密方式
     * @param $pass
     * @return string
     */
    function prePass($pass){
        return getHash($pass);
    }

    function register($user, $password, $tel, $email){
        $data['user']=$user;
        $data['code']=getPassWord($user,$password);
        $data['tel']=$tel;
        $data['email']=$email;
        return $this->db->insert("user",$data)>0;
    }

    /**
     * @param string $loginName 登录名
     * @param string $pass
     * @param $expanded
     * @return string
     */
    function login($loginName, $pass, $expanded){
        if(empty($loginName)|| empty($pass)){
            return "账号或者用户名为空";
        }
        $loginName=$this->db->quote($loginName);
        $pass=getPassWord($loginName,$pass);
        $pass=$this->db->quote($pass);

        $sql="select id,user,tel,email,level from user ";
        if(isset($expanded)&& $expanded){
            //TODO 这个方式下加密会出错
            $sql.="WHERE code=$pass and (user=$loginName or tel={$loginName} or email={$loginName})";
        }else{
            $sql.="where code=$pass and user=$loginName";
        }
        $re=$this->db->getOne($sql);
        if(!empty($re)){
            $_SESSION['blindApp_user']['login']=true;
            $_SESSION['blindApp_user']['info']=$this->db->getOne($sql);
            return "success";
        }else{
            return "账号或者用户名错误";
        }
    }

    function checkLogin(){
        return isset($_SESSION['blindApp_user']['login'])&& $_SESSION['blindApp_user']['login'];
    }

    function getBaseInfo(){
        if($this->checkLogin()){
            return $_SESSION['blindApp_user']['info'];
        }else{
            return null;
        }
    }

} 